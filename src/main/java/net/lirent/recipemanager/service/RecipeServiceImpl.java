package net.lirent.recipemanager.service;

import net.lirent.recipemanager.exception.RecipeException;
import net.lirent.recipemanager.model.Recipe;
import net.lirent.recipemanager.repository.RecipeRepository;
import net.lirent.recipemanager.utility.Category;
import net.lirent.recipemanager.utility.SearchBuilder;
import net.lirent.recipemanager.utility.Utils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * The RecipeServiceImpl encapsulates all business behaviors for operations
 * on the Recipe entity model and some related entities.
 * </p>
 *
 * @author Lirent
 */

@Service
public class RecipeServiceImpl implements RecipeService{

    public static final String NO_RECIPE_WITH_ID = "No recipe found with id=";
    public static final String NOT_FOUND = "Sorry! We didn't find any results matching this search";
    /**
     * The Spring Data repository for Recipe entities.
     */
    @Autowired
    private RecipeRepository recipeRepository;

    @Override
    public List<Recipe> getAllRecipes() {
        return recipeRepository.findAll();
    }

    @Override
    public List<Recipe> searchRecipes(String title,
                                      Integer servings,
                                      Category category,
                                      List<String> includeIngredients,
                                      List<String> excludeIngredients,
                                      String instructions) {
        Specification<Recipe> recipeSpecification = SearchBuilder.build(title, servings, category, includeIngredients, excludeIngredients, instructions);
        List<Recipe> recipeList = recipeRepository.findAll(recipeSpecification);

        //no recipe found
        if (recipeList.isEmpty())
            throw new RecipeException(NOT_FOUND);

        return recipeRepository.findAll(recipeSpecification);
    }

    @Override
    public Recipe getRecipeById(Long id) {
        var recipe = recipeRepository.findById(id);
        if (recipe.isEmpty())
            throw new RecipeException(NO_RECIPE_WITH_ID + id);

        return recipe.get();
    }

    @Override
    public Recipe addRecipe(Recipe recipe) {
        return recipeRepository.save(recipe);
    }

    @Override
    public Recipe updateRecipe(Recipe recipe, long id) {
        var recipeDb = getRecipeById(id);

        var ignoredFields = Utils.getNullPropertyNames(recipe);
        BeanUtils.copyProperties(recipe, recipeDb, ignoredFields);
        return recipeRepository.save(recipe);
    }

    @Override
    public void deleteRecipe(Long id) {
        recipeRepository.deleteById(id);
    }
}
