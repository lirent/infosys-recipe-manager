package net.lirent.recipemanager.exception;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class RecipeException extends RuntimeException{

    private String message;

    @Override
    public String getMessage() {
        return message;
    }
}
